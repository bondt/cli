---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<!--
This documentation is auto generated by a script.
Please do not edit this file directly, check cmd/gen-docs/docs.go.
-->

# `glab ci run`

Create or run a new CI pipeline

```plaintext
glab ci run [flags]
```

## Examples

```plaintext
glab ci run
glab ci run -b main
glab ci run -b main --variables MYKEY:some_value
glab ci run -b main --variables MYKEY:some_value --variables KEY2:another_value

```

## Options

```plaintext
  -b, --branch string       Create pipeline on branch/ref <string>
      --variables strings   Pass variables to pipeline
```

## Options inherited from parent commands

```plaintext
      --help              Show help for command
  -R, --repo OWNER/REPO   Select another repository using the OWNER/REPO or `GROUP/NAMESPACE/REPO` format or full URL or git URL
```
